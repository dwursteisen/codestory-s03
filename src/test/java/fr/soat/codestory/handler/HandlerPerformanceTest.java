package fr.soat.codestory.handler;

import elevator.*;
import elevator.engine.ElevatorEngine;
import elevator.exception.ElevatorIsBrokenException;
import fr.soat.codestory.type.Action;
import org.junit.Test;

import java.util.*;

/**
 * Created by david on 22/09/13.
 */
public class HandlerPerformanceTest {

    private Set<ElevatorHandler> toTest = new HashSet<>(Arrays.asList(new EmptyHandler(), new StupidHandler()));

    @Test
    public void should_find_best_handlers() {
        Map<ElevatorHandler, Integer> scoresMap = new HashMap<>(toTest.size());
        for (ElevatorHandler handler : toTest) {
            Integer score = simulation(handler);
            scoresMap.put(handler, score);
        }

        scoresMap.entrySet().stream()                                              // \n
                .sorted((e1, e2) -> Integer.compare(e1.getValue(), e2.getValue())) // \n
                .forEach(e -> System.out.println(   // \n
                        e.getKey().getClass().getSimpleName() + " --> " + e.getValue() // \n
                ));
    }

    private Integer simulation(ElevatorHandler handler) {
        try {
            Building building = new Building(new ElevatorEngineBridge(handler), new ConstantMaxNumberOfUsers());
            Score score = new Score();
            for (int i = 0; i < 5000; i++) {
                onTick(building, score);
            }
            return score.score;
        } catch (Exception ex) {
            System.err.println("Oups, got an exception ! Handler should be invalid");
            ex.printStackTrace();
            return -1;
        }
    }

    private void onTick(Building building, Score score) {
        try {
            building.addUser();
            Set<User> doneUsers = building.updateBuildingState();
            for (User doneUser : doneUsers) {
                score.success(doneUser);
            }
        } catch (ElevatorIsBrokenException e) {
            throw new RuntimeException("Elevator just been reseted...", e);
        }
    }

    private static class ElevatorEngineBridge implements ElevatorEngine {

        private final ElevatorHandler handler;

        private ElevatorEngineBridge(ElevatorHandler handler) {
            this.handler = handler;
        }

        @Override
        public ElevatorEngine call(Integer floor, Direction direction) throws ElevatorIsBrokenException {
            handler.call(floor, toDirection(direction));
            return this;
        }

        private fr.soat.codestory.type.Direction toDirection(Direction direction) {
            return direction.equals(Direction.UP) ? fr.soat.codestory.type.Direction.UP : fr.soat.codestory.type.Direction.DOWN;
        }

        @Override
        public ElevatorEngine go(Integer floor) throws ElevatorIsBrokenException {
            handler.go(floor);
            return this;
        }

        @Override
        public Command nextCommand() throws ElevatorIsBrokenException {
            Action action = handler.nextCommand();
            return toCommand(action);
        }

        private Command toCommand(Action action) {
            switch (action) {
                case CLOSE:
                    return Command.CLOSE;
                case DOWN:
                    return Command.DOWN;
                default:
                case NOTHING:
                    return Command.NOTHING;
                case UP:
                    return Command.UP;
                case OPEN:
                    return Command.OPEN;
            }
        }

        @Override
        public ElevatorEngine userHasEntered(User user) throws ElevatorIsBrokenException {
            handler.userHasEntered();
            return this;
        }

        @Override
        public ElevatorEngine userHasExited(User user) throws ElevatorIsBrokenException {
            handler.userHasExited();
            return this;
        }

        @Override
        public ElevatorEngine reset(String cause) throws ElevatorIsBrokenException {
            handler.reset(cause);
            return this;
        }
    }
}
