package fr.soat.codestory;

import fr.soat.codestory.handler.ElevatorHandler;
import fr.soat.codestory.type.Action;
import fr.soat.codestory.type.Direction;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.Mockito.*;

/**
 * Created by david on 21/09/13.
 */
public class CodeStoryServerTest {


    private HttpServletRequest request;
    private HttpServletResponse response;
    private ElevatorHandler handler;
    private CodeStoryServer server;

    @Before
    public void setUp() throws Exception {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        Mockito.doReturn(Mockito.mock(PrintWriter.class)).when(response).getWriter();
        handler = mock(ElevatorHandler.class);
        server = new CodeStoryServer(handler);
    }

    @Test
    public void should_call_reset() throws ServletException, IOException {
        doReturn("/reset").when(request).getPathInfo();
        doReturn("cause").when(request).getParameter("cause");
        server.doGet(request, response);
        verify(handler).reset("cause");
    }

    @Test
    public void should_call_next_command() throws ServletException, IOException {
        doReturn("/nextCommand").when(request).getPathInfo();
        doReturn(Action.NOTHING).when(handler).nextCommand();
        server.doGet(request, response);
        verify(handler).nextCommand();
        verify(response.getWriter()).println("NOTHING");
    }

    @Test
    public void should_call_call() throws ServletException, IOException {
        doReturn("/call").when(request).getPathInfo();
        doReturn("666").when(request).getParameter("atFloor");
        doReturn("UP").when(request).getParameter("to");
        server.doGet(request, response);
        verify(handler).call(666, Direction.UP);
    }

    @Test
    public void should_call_go() throws ServletException, IOException {
        doReturn("/go").when(request).getPathInfo();
        doReturn("666").when(request).getParameter("floorToGo");
        server.doGet(request, response);
        verify(handler).go(666);
    }

    @Test
    public void should_call_user_entered() throws ServletException, IOException {
        doReturn("/userHasEntered").when(request).getPathInfo();
        server.doGet(request, response);
        verify(handler).userHasEntered();
    }

    @Test
    public void should_call_user_exited() throws ServletException, IOException {
        doReturn("/userHasExited").when(request).getPathInfo();
        server.doGet(request, response);
        verify(handler).userHasExited();
    }
}
