package fr.soat.codestory.type;

/**
 * Created by david on 21/09/13.
 */
public enum Action {
    NOTHING, UP, DOWN, OPEN, CLOSE;
}
