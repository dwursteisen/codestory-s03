package fr.soat.codestory.handler;

import fr.soat.codestory.type.Action;
import fr.soat.codestory.type.Direction;

/**
 * Created by david on 21/09/13.
 */
public interface ElevatorHandler {
    void call(int floor, Direction direction);

    void reset(String cause);

    void go(int floor);

    void userHasEntered();

    void userHasExited();

    Action nextCommand();
}
