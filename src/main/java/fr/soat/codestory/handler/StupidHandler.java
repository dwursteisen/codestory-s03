package fr.soat.codestory.handler;

import fr.soat.codestory.type.Action;
import fr.soat.codestory.type.Direction;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by david on 21/09/13.
 */
public class StupidHandler implements ElevatorHandler {

    private final List<Action> actions = Arrays.asList(
            Action.OPEN, Action.CLOSE, Action.UP, // \n
            Action.OPEN, Action.CLOSE, Action.UP, // \n
            Action.OPEN, Action.CLOSE, Action.UP, // \n
            Action.OPEN, Action.CLOSE, Action.UP, // \n
            Action.OPEN, Action.CLOSE, Action.UP, // \n
            Action.OPEN, Action.CLOSE, Action.DOWN, // \n
            Action.OPEN, Action.CLOSE, Action.DOWN, // \n
            Action.OPEN, Action.CLOSE, Action.DOWN, // \n
            Action.OPEN, Action.CLOSE, Action.DOWN, // \n
            Action.OPEN, Action.CLOSE, Action.DOWN // \n
    );
    private Iterator<Action> currentAction = actions.iterator();

    @Override
    public void call(int floor, Direction direction) {
        // nothing to do
    }

    @Override
    public void reset(String cause) {
        System.err.println(String.format("Oups ! Got reset because '%s'", cause));
        currentAction = actions.iterator();
    }

    @Override
    public void go(int floor) {
        // nothing to do
    }

    @Override
    public void userHasEntered() {
        // nothing to do
    }

    @Override
    public void userHasExited() {
        // nothing to do
    }

    @Override
    public Action nextCommand() {
        if (!currentAction.hasNext()) {
            currentAction = actions.iterator();
        }
        return currentAction.next();
    }
}
