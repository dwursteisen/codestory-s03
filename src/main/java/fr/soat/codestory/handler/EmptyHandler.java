package fr.soat.codestory.handler;

import fr.soat.codestory.type.Action;
import fr.soat.codestory.type.Direction;

/**
 * Created by david on 21/09/13.
 */
public class EmptyHandler implements ElevatorHandler {

    @Override
    public void call(int floor, Direction direction) {

    }

    @Override
    public void reset(String cause) {

    }

    @Override
    public void go(int floor) {

    }

    @Override
    public void userHasEntered() {

    }

    @Override
    public void userHasExited() {

    }

    @Override
    public Action nextCommand() {
        return Action.NOTHING;
    }
}
