package fr.soat.codestory;

import fr.soat.codestory.handler.ElevatorHandler;
import fr.soat.codestory.handler.EmptyHandler;
import fr.soat.codestory.type.Direction;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by david on 21/09/13.
 */
public class CodeStoryServer extends HttpServlet {

    private final ElevatorHandler elevatorHandler;

    public CodeStoryServer(ElevatorHandler elevatorHandler) {
        this.elevatorHandler = elevatorHandler;
    }

    public static void main(String[] args) throws Exception {
        ElevatorHandler handler;
        if (args.length > 0) {
            handler = ElevatorHandler.class.cast(Class.forName(args[0]).newInstance());
        } else {
            handler = new EmptyHandler();
        }
        Server server = new Server(1337);
        ServletContextHandler context = new ServletContextHandler(server, "/");
        context.addServlet(new ServletHolder(new CodeStoryServer(handler)), "/*");
        server.setHandler(context);
        server.start();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        switch (pathInfo) {
            case "/nextCommand":
                resp.getWriter().println(elevatorHandler.nextCommand());
                break;
            case "/reset":
                elevatorHandler.reset(req.getParameter("cause"));
                break;
            case "/call":
                Direction direction = Direction.valueOf(req.getParameter("to"));
                Integer floor = Integer.valueOf(req.getParameter("atFloor"));
                elevatorHandler.call(floor, direction);
                break;
            case "/userHasExited":
                elevatorHandler.userHasExited();
                break;
            case "/userHasEntered":
                elevatorHandler.userHasEntered();
                break;
            case "/go":
                Integer floorToGo = Integer.valueOf(req.getParameter("floorToGo"));
                elevatorHandler.go(floorToGo);
                break;

        }

    }
}
