# CodeStory S03 #

Bienvenue dans l'aventure CodeStory Saison 03


Ce projet simule "l'inteligence" d'un ascenceur.


Ce projet nécessite :
---

* de lancer une fois code elevator
* maven 3
* jdk 8 developer preview

Pour démarrer le projet
--

le Main est dans CodeStoryServer

Architecture du projet
---
Tous les appels de la servlet sont redirigé vers un ElevatorHandler
qui représente un algorithme d'assenceur. Implémenté votre ElevatorHandler
pour ajouter votre algorithme.

Vous pouvez lancer votre main avec, en argument, la class de votre handler
pour le lancer automatiquement au démarrage.

Tests
---
HandlerPerformanceTest lance une simulation sur une liste de handler et
tri ensuite les scores. Ceci devrait permettre de connaitre
l'algorithme le plus perfomant.